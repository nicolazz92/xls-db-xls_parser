DROP TABLE IF EXISTS user;
DROP TABLE IF EXISTS workplace;
DROP SEQUENCE IF EXISTS glob_seq;
CREATE SEQUENCE glob_seq START WITH 10;
CREATE TABLE user ( id INT PRIMARY KEY AUTO_INCREMENT, name VARCHAR(50), birthday DATE);
INSERT INTO user (name, birthday) VALUES ('Peter', '1991-11-04'), ('Helen', '1990-05-07');
CREATE  TABLE workplace ( id INT DEFAULT glob_seq.nextval PRIMARY KEY , name VARCHAR(50), workplace VARCHAR(100), FOREIGN KEY (name) REFERENCES user(name) ON DELETE CASCADE );
INSERT INTO workplace(name, workplace) VALUES ('Peter', 'Lanit'), ('Helen', 'Epam');