package sbt.mapper;

import org.apache.ibatis.annotations.Mapper;
import sbt.domain.User;

/**
 * Created by nicolas
 * on 14.03.2017, 13:54.
 */
@Mapper
public interface UserMapper {

    User selectUserById(int id);

}
