package sbt.mapper;

import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * Created by nicolas
 * on 15.03.2017, 13:08.
 */
@Mapper
public interface JoinMapper {

    List<Map> selectNameBirthdayWorkplace();

    List<Map> getAllUsers();

    List<Map> getUserById(int id);

}
