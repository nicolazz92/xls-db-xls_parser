package sbt.service;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by nicolas
 * on 15.03.2017, 14:46.
 */
@Service
public class XLSWriter {

    /**
     * Записывает данные из запроса в XLSX файл
     *
     * @param workbookPath   путь к существующей книге или книге, которую ещё только нужно создать
     * @param sheetPath      имя существующего листа или листа, который ещё только предстоит создать
     * @param clearSheetFlag следует ли очистить лист перед записью
     * @param data           данные из запроса
     * @param startRow       начальная строка
     * @param startCell      начальный столбец
     * @throws IOException проблемы при работе с файлами
     */
    public void writeIntoXLSFile(String workbookPath, String sheetPath, boolean clearSheetFlag, List<Map> data, int startRow, int startCell) throws IOException {

        Workbook workbook = getOrCreateWorkbook(workbookPath);
        Sheet sheet = createOrUpdateSheet(sheetPath, workbook, clearSheetFlag);

        Row row = sheet.createRow(startRow);
        Cell cell = row.createCell(startCell);

        writeData(workbook, sheet, row, cell, data);

        persistChanges(workbookPath, workbook);
    }

    /**
     * Если книга по адресу name уже сущестует, открывает её, если нет - создаёт новую и открывает.
     *
     * @param name путь и имя книги
     * @return рабочую переменную книги
     * @throws IOException проблемы при создании файла новой книги
     */
    private Workbook getOrCreateWorkbook(String name) throws IOException {
        File bookFile = new File(name);
        if (bookFile.exists()) {
            return new XSSFWorkbook(new FileInputStream(bookFile));
        }

        Workbook result = new XSSFWorkbook();
        try (FileOutputStream fileOut = new FileOutputStream(name)) {
            result.write(fileOut);
            System.out.println("Book " + fileOut + " created");
        }

        return result;
    }

    /**
     * Сохраняет изменения в книге на диск
     *
     * @param pathToBookFile путь к файлу книги
     * @param workbook       переменная работы с книгой
     */
    private void persistChanges(String pathToBookFile, Workbook workbook) {
        try (FileOutputStream fileOutputStream = new FileOutputStream(pathToBookFile)) {
            workbook.write(fileOutputStream);
            System.out.println("Book updated");
            workbook.close();
        } catch (FileNotFoundException e) {
            System.out.println("Can't save changes into book file");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Возвращает существующий или создаёт новый лист книги
     *
     * @param sheetName      имя листа
     * @param workbook       рабочая переменная книги
     * @param clearSheetFlag очищать ли лист
     * @return рабочий лист
     */
    private Sheet createOrUpdateSheet(String sheetName, Workbook workbook, boolean clearSheetFlag) {
        removeSheet(workbook, workbook.getSheet(sheetName), clearSheetFlag);

        Sheet sheet = workbook.getSheet(sheetName);

        if (sheet == null) {
            sheet = workbook.createSheet(sheetName);
            System.out.println("Sheet " + sheetName + " created");
            return sheet;
        }
        return sheet;
    }

    /**
     * удаляет лист, если clearSheetFlag == true
     *
     * @param workbook       переменная книги
     * @param sheet          лист
     * @param clearSheetFlag флаг на очищение
     */
    private void removeSheet(Workbook workbook, Sheet sheet, boolean clearSheetFlag) {
        if (clearSheetFlag && sheet != null) {
            String sheetName = sheet.getSheetName();
            int index = workbook.getSheetIndex(sheet);
            workbook.removeSheetAt(index);
            System.out.println("Sheet " + sheetName + " removed");
        }
    }

    /**
     * Записывает данные из запроса в определённые столбцы таблицы
     *
     * @param sheet    рабочий лист таблицы
     * @param row      начальная строка
     * @param cell     начальная колонка
     * @param data     данные
     * @param workbook рабочая переменная книги
     */
    private void writeData(Workbook workbook, Sheet sheet, Row row, Cell cell, List<Map> data) {

        //список колонок(полей запроса)
        List<String> attributes = writeColumnNames(workbook, data, row, cell);

        Row workRow = sheet.createRow(row.getRowNum() + 1);
        Cell workCell = workRow.createCell(cell.getColumnIndex());
        for (Map user : data) {
            for (String field : attributes) {
                workCell.setCellValue(user.get(field).toString());
                workCell = workRow.createCell(workCell.getColumnIndex() + 1);
            }
            workRow = sheet.createRow(workRow.getRowNum() + 1);
            workCell = workRow.createCell(cell.getColumnIndex());
        }
    }

    /**
     * Запись имён колонок. Функция делает запись одной верхней строки таблицы, куда пишет имена атрибутов.
     * Пример:
     * _________________________
     * |birthday|name|workplace|
     * -------------------------
     *
     * @param workbook рабочая переменная книги
     * @param data     данные из запроса
     * @param row      начальная строка таблицы
     * @param cell     начальная колонка таблицы
     * @return список с именами атрибутов, чтобы потом можно было в определённом этим списком порядке
     * записывать данные из запроса в нужные колонки таблицы.
     */
    private List<String> writeColumnNames(Workbook workbook, List<Map> data, Row row, Cell cell) {
        Set dataAttributes = data.iterator().next().keySet();
        Cell workCell = row.createCell(cell.getColumnIndex());
        List<String> attributes = new ArrayList<>();

        CellStyle style = createAttributesCellStyle(workbook);

        for (Object field : dataAttributes) {
            workCell.setCellValue(field.toString());
            workCell.setCellStyle(style);
            attributes.add(field.toString());
            workCell = row.createCell(workCell.getColumnIndex() + 1);
        }

        return attributes;
    }

    /**
     * Создаёт стиль для функции writeColumnNames
     *
     * @param workbook переменная рабочей книги
     * @return стиль
     */
    private CellStyle createAttributesCellStyle(Workbook workbook) {
        CellStyle style = workbook.createCellStyle();
        Font font = workbook.createFont();
        font.setBold(true);
        style.setFont(font);
        return style;
    }
}
