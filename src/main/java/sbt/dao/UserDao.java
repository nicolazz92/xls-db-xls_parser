package sbt.dao;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;
import sbt.domain.User;

/**
 * Created by nicolas
 * on 14.03.2017, 15:10.
 */
@Repository
public class UserDao {

    private final SqlSession sqlSession;

    public UserDao(SqlSession sqlSession) {
        this.sqlSession = sqlSession;
    }

    public User findByName(String name) {
        return this.sqlSession.selectOne("selectUserByName", name);
    }

}
