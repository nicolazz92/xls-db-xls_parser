package sbt.domain;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * Created by nicolas
 * on 15.03.2017, 12:57.
 */
public class JoinResult implements Serializable{

    private static final long serialVersionUID = 1L;

    private String name;

    private LocalDate birthday;

    private String workplace;

    public JoinResult() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public String getWorkplace() {
        return workplace;
    }

    public void setWorkplace(String workplace) {
        this.workplace = workplace;
    }
}
