package sbt.domain;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * Created by nicolas
 * on 14.03.2017, 13:34.
 */
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    private int id;

    private String name;

    private LocalDate birthday;

    public User() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", birthday=" + birthday +
                '}';
    }
}
