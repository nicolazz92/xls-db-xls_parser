package sbt;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import sbt.dao.UserDao;
import sbt.mapper.JoinMapper;
import sbt.mapper.UserMapper;
import sbt.service.XLSWriter;

@SpringBootApplication
public class StudyApplication implements CommandLineRunner {

    final private UserMapper userMapper;
    final private UserDao userDao;
    final private JoinMapper joinMapper;
    final private XLSWriter xlsWriter;

    public StudyApplication(UserMapper globalMapper, UserDao userDao, JoinMapper joinMapper, XLSWriter xlsWriter) {
        this.userMapper = globalMapper;
        this.userDao = userDao;
        this.joinMapper = joinMapper;
        this.xlsWriter = xlsWriter;
    }

    public static void main(String[] args) {
        SpringApplication.run(StudyApplication.class, args);
    }

    @Override
    public void run(String... strings) throws Exception {
//        System.out.println(userMapper.selectUserById(1));
//        System.out.println(userDao.findByName("Helen"));
        System.out.println(joinMapper.selectNameBirthdayWorkplace());
        xlsWriter.writeIntoXLSFile("C://test.xlsx", "Users", true, joinMapper.selectNameBirthdayWorkplace(), 0, 0);

        System.out.println(joinMapper.getAllUsers());
        xlsWriter.writeIntoXLSFile("C://test.xlsx", "Users", false, joinMapper.getAllUsers(), 4, 0);

        System.out.println(joinMapper.getUserById(1));
        xlsWriter.writeIntoXLSFile("C://test.xlsx", "Users", false, joinMapper.getUserById(1), 8, 0);
    }
}
